//================================================================== connect to central server ==============================================================
//Promise readfile server.config
const readConfigFile = () => {
    return new Promise((resolve, reject) => {
        var fs = require('fs');
        var contents = fs.readFileSync('server.config', 'utf8');
        var conSplit = contents.split("\n");
        if(contents.length >= 2){    
            var ipV4RegEx = /^(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))\.(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))\.(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))\.(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))$/;
            var portRegEx = /^([0-9]{1,5})$/;
            if(ipV4RegEx.test(conSplit[0]) && portRegEx.test(conSplit[1])){
                var config = {
                    ip: conSplit[0],
                    port: parseInt(conSplit[1])
                }
                resolve(config);
            }else{
                reject('error unsupport server config format.');
            }
        }else{
            reject('error unsupport server config format.');
        }
    });
};

//promise get ip from api
const get_ip_info = () => {
    return new Promise((resolve, reject) => {
        const request = require('request');
        request('https://ipinfo.io/json', { json: true }, (err, res, body) => {
            if(err){
                reject(err);
            }else{
                resolve(body);
            }
        });
    });
};

const get_client_con_to_central = function (io, user, pass, server_central_config, client_config){
    if(pass === user.substring(6, 10)){
        //connect to central server for get online users
        var net = require('net');
        var client = new net.Socket();
        var payload = {
            users : new Array()
        };

        client.connect(server_central_config.port, server_central_config.ip, function() {
            console.log('Connected, user : %s at %s:%d ', user, client_config.ip, client_config.port);
            var str = 'USER:' + user +'\nPASS:' + pass + '\nIP:'+client_config.ip+'\nPORT:' + client_config.port;
            client.write(str);
        });
            
        client.on('data', function(data) {
            var dataStr = data + '';
             //console.log('Server: ' + dataStr);
             if(dataStr === '200 SUCCESS\n' || dataStr === '404 ERROR\n' || dataStr === 'Hello ' + user){                 
                console.log('Server(to %s): %s', user, dataStr);
                if(dataStr === 'Hello ' + user){
                    console.log('Client(%s) : Hello Server', user);
                    client.write('Hello Server');
                    payload.users = new Array();
                }          
            }else{
                dataStr.split("\n").forEach(item => {
                    item = item.trim();
                    if(item != 'END' && item != '\n' && item != ''){
                        var strArr = item.split(":");
                        var user = {
                            username: strArr[0],
                            ip: strArr[1],
                            port: parseInt(strArr[2])
                        }
                        payload.users.push(user);                     
                    }else if(item == 'END'){
                        client.userList = payload.users;
                        io.emit('users', payload); //send users online update 
                        console.log('Server(chat-backend) : send users-list to client'); 
                    }
                });                                
            }
        });
            
        client.on('close', function() {
            console.log('Connection %s closed', user);
        });
            
        client.on('error', function (err) {
            console.error('Central Server (Talk-To-U) Error!!');
            console.error(JSON.stringify(err));
        });

        return client;
    }
    //auth error
    return null; 
};

const get_server_chat = function (io, socket_io, user_data){
    var net = require('net');
    var server = net.createServer();
    var client = null;
    server.on('connection', function(socket){
        var clientAddress = socket.remoteAddress + ':' + socket.remotePort;
        console.log('Server (' + user_data.username + ') New Client connection at :' + clientAddress);
        console.log('Server has client chat ? : ' +socket_io.chat_object.client != null);
        console.log('Server has client in server ? : ' + client != null);
        if(socket_io.chat_object.client == null && client == null){ 
            client = socket;
            socket.on('data', function(data){
                console.log('Server(%s) Receive Data(%s) : %s' ,user_data.username , clientAddress, data);
                var payload = {
                    username : clientAddress,
                    messages: data + ''
                }
                io.emit('chat_' + user_data.username , payload); 
            });

            socket_io.on('chat_' +user_data.username, function(data){
                io.emit('chat_' + user_data.username , data); 
                console.log('Data from %s to %s : %s' , user_data.username, clientAddress, data.messages);
                if(client != null){
                    socket.write(data.messages);
                }
            });

            socket.once('close', function(){
                var payload = {
                    username : clientAddress,
                    messages: 'Closed Connection'
                }
                io.emit('chat_' + user_data.username , payload); 
                console.log('Connetion from %s closed.', clientAddress);
                client = null;
            });
        }else{
            console.log('Reject connection at :' + clientAddress + '(USER BUSY)');
            socket.write('REJECT : USER BUSY');
            socket.destroy();
        }
    });

    server.listen(0, function(){
        console.log('server listening at %j', server.address());
    });

    server.on('close', function(){
        console.log('Server chat %s close.', user_data.username);
    });

    server.on('error', function(error){
        console.error(JSON.stringify(error));
    });

    server.DestroyServer = function(){
        if(client != null){
            client.destroy();
        }
    };

    return server;
};

const get_client_chat = function (io, socket_io, des_ip, des_port, des_name, user_data){
    var net = require('net');
    var client = new net.Socket();
    client.canConnect = true;

    client.connect(des_port, des_ip, function() {
        console.log('User : %s Connect to %s complete.', user_data.username, des_name);
        console.log('IP: %s PORT: %d', des_ip, des_port);
        client.write('Hello ' + des_name);
    });

    client.on('data', function(data) {
        console.log('server(%s) to %s : %s', des_name, user_data.username, data);
        var payload = {
            username : des_name,
            messages: data + ''
        }
        io.emit('chat_' + user_data.username , payload); 
    });

    socket_io.on('chat_' + user_data.username, function(data){
        io.emit('chat_' + user_data.username , data); 
        console.log('Client(%s) to %s : %s', user_data.username, des_name, data.messages);
        client.write(data.messages);
    });


    client.on('end' ,function() {
        socket_io.chat_object.client = null;
        console.log('Connection to ' + des_name + ' closed');
        io.emit('chat_' + user_data.username, {
            username: des_name,
            messages: 'CLOSE_CONNECTION!!'
        });
    });

    client.on('error', function (err) {
        client.canConnect = false;
        console.error(JSON.stringify(err));
    });

    return client;
};

//=========================================================== Web ============================================================
get_ip_info().then(res => {
    if (res) {  
        readConfigFile().then( res2 => {
            if(res2){
                var express = require('express');
                var app = express();
                var path = require('path');
                var port = 8080;
                var server = app.listen(port, function() {
                    console.log('Listening on port: ' + port);
                });
                var io = require('socket.io').listen(server);
                
                app.set('views', path.join(__dirname, 'views'));
                app.set('view engine', 'jade'); 
                app.use(express.static('public'));

                app.get('/', function(req, res) {
                    res.render('chat');
                });
                
                io.on('connection', function(socket) {
                    //console.log('a user connected : '+ socket.id);

                    socket.on('auth', function(data) { 
                        var server_central_config = {
                            ip: res2.ip,
                            port: res2.port
                        };
                        var server_chat = get_server_chat(io, socket, data);
                        var client_config = {
                            ip: res.ip,
                            port: server_chat.address().port
                        }
                        socket.chat_object = {
                            owner: data.username,
                            server: server_chat,
                            client: null
                        };
                        var client = get_client_con_to_central(io, data.username, data.password, server_central_config, client_config);
                        if(client != null){
                            socket.client_data = client; //add client object to socket property
                            io.emit('auth_' + data.username, {
                                username: data.username,
                                status: 200,
                                client: client_config 
                            }); 

                            socket.on('client_' + + data.username, function(frient_data){
                                if(socket.chat_object.client != null){
                                    socket.chat_object.client.destroy();
                                }
                                var friend_username = frient_data + '';
                                client.userList.forEach(function(user){
                                    if(user.username === friend_username){
                                        var ip = user.ip;
                                        var port = user.port;
                                        var client_chat = get_client_chat(io, socket, ip, port, friend_username, data);
                                        var delay = 2000;
                                        setTimeout(function() {
                                            socket.chat_object.client = client_chat;
                                            hasConected = client_chat.canConnect;
                                            var code = 'REJECT';
                                            if(hasConected){
                                                code = 'ACCEPT';
                                            }else{
                                                socket.chat_object.client = null;
                                            }
                                            io.emit('client_' + + data.username, {
                                                connect_to: friend_username,
                                                status: code
                                            }); 
                                        }, delay);
                                        return;
                                    }
                                });
                            });
                        }else{
                            socket.chat_object.server.close();
                            socket.chat_object.client.destroy();
                            delete socket.chat_object;
                            //auth error
                            io.emit('auth_' + data.username, {
                                username: data.username,
                                status: 401
                            }); 
                        }
                    });

                    socket.on('disconnect', function() {
                        if(socket.client_data){
                            socket.client_data.destroy();
                            if(socket.chat_object.client != null){
                                socket.chat_object.client.destroy();
                            }
                            socket.chat_object.server.DestroyServer();
                            socket.chat_object.server.close();
                        }
                    }); 
                });
            }
        }).catch(e => {
            console.log(e);
        });
    }
}).catch(e => {
    console.log(e);
});