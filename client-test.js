var net = require('net');
var client = new net.Socket();
var i = 0;
client.connect(51655, '192.168.1.37', function() {
    console.log('Connected');
    client.write('Hello Server');
});

client.on('data', function(data) {
    console.log('server : ' + data);
    var delay = (Math.floor(Math.random() * 2) + 1) * 1000;
    console.log('Delay : ' + delay);
    setTimeout(function() {
        client.write(i++);
    }, delay);
});

client.on('close', function() {
    console.log('Connection closed');
});

client.on('error', function (err) {
    console.error(JSON.stringify(err));
});
