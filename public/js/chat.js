var socket = io();
var chatHistory = $("#chat-history");
var inputMessage = $('#chat-message');
var sendMessageBtn = $('#send-message')
var usersList = $('#users-list');
var chatName = $('#chat-name');
var username = prompt("Please enter username:", "5909650219");
var body = $("body");
function sendMessage(){
    var msg = inputMessage.val();
    if(msg){
        console.log('chat_'+ username);
        socket.emit('chat_'+ username, {
            username: username,
            messages: msg + ''
        });
        inputMessage.val('');
    }
    inputMessage.focus();
}
var friend_chat_data = null;

$(document).ready(function () {

	var height = $(window).height();
	$('.left-chat').css('height', (height - 92) + 'px');
	$('.right-header-contentChat').css('height', (height - 163) + 'px');

    inputMessage.attr("disabled", "disabled");

	inputMessage.focus();

    usersList.append(`
        <li>
            <div class="chat-left-detail">
                <p> Loading.. </p>
            </div>
        </li>
    `);

    sendMessageBtn.click(function() {
        if(friend_chat_data != null){
            sendMessage();
        }else{
            chatHistory.append(`
                <li>
                    <div class="rightside-left-chat">
                        <span><i class="fa fa-circle" aria-hidden="true"></i> System Admin <small>10:00 AM,Today</small> </span><br><br>
                        <p>ท่านต้องเริ่มการสนทนาก่อน จึงจะสามารถส่งข้อความได้</p>
                    </div>
                </li>
            `);
            chatHistory[0].scrollTop = chatHistory[0].scrollHeight;
        }
    });

    inputMessage.keyup(function() {
       if(event.which == 13){
            sendMessage();
       }
    });
    
	socket.on('chat_' + username, function (data) {
        if(data.messages === 'Closed Connection'){
            inputMessage.attr("disabled", "disabled");
            chatHistory.append(`
                <li>
                    <div class="rightside-left-chat">
                        <span><i class="fa fa-circle" aria-hidden="true"></i> ${data.username} <small>10:00 AM,Today</small> </span><br><br>
                        <p>${data.messages}</p>
                    </div>
                </li>
            `);
            return;
        }
        inputMessage.removeAttr("disabled"); 
        if(friend_chat_data === null){
            chatHistory.empty();
            friend_chat_data = data.username;
            chatName.text(friend_chat_data);
        }else{
            if (friend_chat_data != data.username && data.username != username) {
                chatHistory.empty();
                friend_chat_data = data.username;
                chatName.text(friend_chat_data);
            }
        }
        var time = new Date().toLocaleTimeString();
        inputMessage.focus();
        if (data.username == username) {
            chatHistory.append(`
                <li>
                    <div class="rightside-right-chat">
                        <span><i class="fa fa-circle" aria-hidden="true"></i> Me <small>${time}</small> </span><br><br>
                        <p>${data.messages}</p>
                    </div>
                </li>
            `);
        } else {
            chatHistory.append(`
                <li>
                    <div class="rightside-left-chat">
                        <span><i class="fa fa-circle" aria-hidden="true"></i> ${data.username} <small>${time}</small> </span><br><br>
                        <p>${data.messages}</p>
                    </div>
                </li>
            `);
        }
        chatHistory[0].scrollTop = chatHistory[0].scrollHeight;
	});

	socket.on('users', function (data) {
        console.log(data);
        usersList.empty();
        var time = new Date().toLocaleTimeString();
        usersList.append(`
            <li>
                <div class="chat-left-detail">
                    <p>Last update at ${time} </p>
                </div>
            </li>
        `);
		data.users.forEach(item => {
            if(item.username != username){
                // var span_status = '<span><i class="fa fa-circle" aria-hidden="true"></i> online</span>';
                // if (item.port == -1) {
                //     span_status = '<span><i class="fa fa-circle orange" aria-hidden="true"></i> offline</span>';
                // }
                // usersList.append(`
                //     <li>
                //         <div class="chat-left-img">
                //             <img src="/images/businessman.png">
                //         </div>
                //         <div class="chat-left-detail">
                //             <p>${item.username}</p>
                //             ${span_status}
                //         </div>
                //     </li>
                // `);
                if (item.port != -1) {
                    var span_status = '<span><i class="fa fa-circle" aria-hidden="true"></i> online</span>';
                    usersList.append(`
                        <li>
                            <div class="chat-left-img">
                                <img src="/images/businessman.png">
                            </div>
                            <div class="chat-left-detail">
                                <p>${item.username}</p>
                                ${span_status}
                            </div>
                        </li>
                    `);
                }
            }
        });
        usersList.on("click","li", function(){
            var frient_username = $(this).find("p").text().trim();
            var status = $(this).find("span").text().trim();
            if(status != 'offline'){
                socket.emit('client_' + username, frient_username);
                inputMessage.attr("disabled", "disabled");
                body.addClass("loading"); //add modal waiting       
            }   
         });
    });

    socket.on('client_' + username, function (data) {
        body.removeClass("loading");
        chatHistory.empty();
        friend_chat_data = data.connect_to + '';
        chatName.text(friend_chat_data);
        var time = new Date().toLocaleTimeString();
        if(data.status == 'ACCEPT'){
            inputMessage.removeAttr("disabled"); 
            chatHistory.append(`
                <li>
                    <div class="rightside-right-chat">
                        <span><i class="fa fa-circle" aria-hidden="true"></i> Me <small>${time}</small> </span><br><br>
                        <p>Hello ${friend_chat_data}</p>
                    </div>
                </li>
            `);
        }else{
            alert("Can't Connect to " + friend_chat_data);
            chatHistory.append(`
                <li>
                    <div class="rightside-left-chat">
                        <span><i class="fa fa-circle" aria-hidden="true"></i> ${friend_chat_data} <small>${time}</small> </span><br><br>
                        <p>Reject Connection</p>
                    </div>
                </li>
            `);
        }
    });
    
    if(username != null || username != '5909xxxxxx' || username != ''){
        var data = {
            username: username,
            password: username.substring(6, 10)
        };
        socket.emit('auth', data);
    }
    
    socket.on('auth_' + username, function (data) {
        console.log(JSON.stringify(data));
        alert(JSON.stringify(data));
    });
});