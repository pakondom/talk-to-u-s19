//node js tcp server test
var net = require('net');
var server = net.createServer();
server.on('connection', function(socket){
    socket.setTimeout(3000);
    var clientAddress = socket.remoteAddress + ':' + socket.remotePort;
    console.log('New Client connection at :' + clientAddress);

    socket.on('data', function(data){
        console.log('Receive Data(%s) : %s', clientAddress, data);
        socket.write('receive : ' + data);
    });

    socket.once('close', function(){
        console.log('Connetion from %s closed.', clientAddress);
    });

    socket.on('error', function(err){
        console.log('Connetion from %s error : $s', clientAddress, err);
    });

    socket.on('timeout', function(){
        console.log('Connetion from %s timeout.', clientAddress);
        socket.end();
    });
});

server.listen(9000, function(){
    console.log('server listening at %j', server.address());
});

server.close(function(){
    console.log('Server close');
});